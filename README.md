# gitlab-vpn (Not Finished)

GitLab VPN is for establishing a virtual private network in GitLab
internal microservices and end clients.

## When it's useful

Given docker executor of GitLab Runner happens on a closed network, it's hard to
access the node directly. With GitLab VPN, you can make these containers to join
VPN in order to put the node in the same network with the end client's network
space.

For example, this service is useful in these scenarios:

- Make GitLab Runner serve Review App.
- Debug a job on GitLab Runner.

## How to make GitLab Runner serve Review App.

GitLab Runner side:

1. Runner picked a job
1. Connect to GitLab VPN with `gitlab-vpn-client`
1. (Optional) If you want to ssh to the container, run ssh daemon and copy the clinet machine's private/public key.
1. Run a job (i.e. server)
1. Sleep the main thread (e.g. `sleep(3600)`), otherwise the job will be terminated.

Client side (Your machine):

1. Connect to GitLab VPN with `gitlab-vpn-client`
1. Visit the review app with the specified alias `http://#{alias_on_vpn}`.

## Run gitlab-vpn server

Execute the `gitlab-vpn-server` binary.

- Options:
  - `-listen-http` ... Listen IP and PORT
  - `-config-file` ... Configuration file (.ovpn)
  - `-auth-server` ... The endpoint of the authorization server (GitLab Rails)
  - `-auth-secret` ... The secret for the authorization server (GitLab Rails)

Example:

```shell
gitlab-vpn-server -listen-http localhost:7878 \
                  -config-file ./gitlab-vpn.ovpn \
                  -auth-server http://gitlab.com \
                  -auth-secret 'abc123'
```

## Connect to the gitlab-vpn server (OpenVPN)

Execute the `gitlab-vpn-client` binary.

- Options:
  - `-vpn-server` ... The address of the gitlab-vpn-server
  - `-project-id` ... The ID of the project
  - `-token` ... GitLab Personal Access Token or `$CI_JOB_TOKEN`.
  - `-alias` ... The network alias after it's connected to a VPN.

```shell
gitlab-vpn-client -vpn-server http://gitlab-vpn.com \
                  -project-id 123 \
                  -token '123abc' \
                  -alias runner-abc 
```

## Authorization and detailed process

1. A node executes `gitlab-vpn-client` with correct parameters.
1. GitLab VPN authorizes the request through GitLab Rails.
  1. `GET http://gitlab.com/api/v4/projects/:project_id/vpn/connect` (Header: `PRIVATE-TOKEN: 1234567abcdefg`)
  1. If the node has permission to the project and VPN, returns 200.
  1. If the node does not has permission to neigther the project nor VPN, returns 400.
1. GitLab VPN establishes the connection on VPN and assign a unique ip address to the node

## IP assignments on VPN

After connected to VPN, each node will be assigned to IPv6 address according to the following rule:

fdXX:XXXX:XXXX:YYYY::/64
- X is unique per project (Allocatable upto 1 to 1,082,432,160,000)
- Y is unique per node (Allocatable upto 1 to 65,025)

Examples:

- Runner-A in Project 1 (fd00:0:1:2) <=> (VPN) <=> Client-A in Project 1 (fd00:0:1:3)
- Runner-B in Project 1 (fd00:0:1:4) <=> (VPN) <=> Client-B in Project 1 (fd00:0:1:5)
- Runner-A in Project 2 (fd00:0:2:2) <=> (VPN) <=> Client-A in Project 2 (fd00:0:2:3)
- Runner-B in Project 2 (fd00:0:2:4) <=> (VPN) <=> Client-B in Project 2 (fd00:0:2:5)
- Runner-A in Project 3 (fd00:0:3.2) <=> (VPN) <=> Client-A in Project 3 (fd00:0:3.3)

## Supported VPN protocol

- OpenVPN

## Author

Shinya Maeda

## License

MIT
